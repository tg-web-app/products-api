create table categories
(
    cid                varchar(255)             not null
        constraint categories_pk
            primary key,
    category_name      varchar(255)             not null
);

create unique index categories_uid_uindex
    on categories (cid);

create table products
(
    pid                varchar(255)             not null
        constraint products_pk
            primary key,
    title              varchar(255)             not null,
    price              integer                  not null,
    description        varchar(255)             not null,
    cid                varchar(255)             not null 
);

create unique index products_uid_uindex
    on products (pid);