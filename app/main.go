package main

import (
	"godb/internal/api"
	pgprovider "godb/internal/pg-provider"
	"godb/internal/service"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
)

func main() {
	ins := pgprovider.New()
	ins.Open()
	ins.Start()

	service := service.NewService(ins)
	router := api.NewRouter(ins, service)
	go func() {
		logrus.Fatal(fasthttp.ListenAndServe(":8080", cors(router.Handle)))
	}()
	logrus.Println("All systems started successfully")

	errChan := make(chan error, 1)
	osSignal := make(chan os.Signal, 1)
	signal.Notify(osSignal, syscall.SIGINT, syscall.SIGTERM)

	select {
	case <-osSignal:
		logrus.Info("All systems closed")
		os.Exit(0)
	case err := <-errChan:
		logrus.Error(err)

		logrus.Info("All systems closed")
		os.Exit(1)
	}
}

func cors(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		ctx.Response.Header.Set("Access-Control-Allow-Headers", "Content-Type,Accept")
		ctx.Response.Header.Set("Access-Control-Allow-Methods", "OPTIONS,POST,GET")
		ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
		if string(ctx.Method()) == fasthttp.MethodOptions {
			ctx.Response.SetStatusCode(200)
			return
		}
		next(ctx)
	}
}
