package service

import (
	"context"
	"godb/internal/models"
	pgprovider "godb/internal/pg-provider"
)

type Service struct {
	pgProvider *pgprovider.Instance
}

func NewService(pgProvider *pgprovider.Instance) *Service {
	return &Service{
		pgProvider: pgProvider,
	}
}

func (s *Service) GetProducts() []models.ProductsResponce { //TODO: оптимизировать алгоритм
	resp := make([]models.ProductsResponce, 0)
	respMap := make(map[int]models.ProductsResponce)
	categories := s.pgProvider.GetAllCategories(context.Background())
	products := s.pgProvider.GetAllProducts(context.Background())

	for _, cat := range categories {
		pr := models.ProductsResponce{
			CategoryId:   cat.CategoryID,
			CategoryName: cat.CategoryName,
			Items:        make([]models.Product, 0),
		}

		respMap[cat.CategoryID] = pr
	}
	for _, p := range products {
		if pr, ok := respMap[p.CategoryID]; ok {
			products := pr.Items
			products = append(products, p)
			pr.Items = products
			respMap[p.CategoryID] = pr
		}
	}

	for _, pr := range respMap {
		resp = append(resp, pr)
	}

	return resp
}

func (s *Service) GetProductsByCategory(category models.Category) []models.Product {
	resp := make([]models.Product, 0)
	products := s.pgProvider.GetAllProducts(context.Background())

	for _, p := range products {
		if p.CategoryID == category.CategoryID {
			resp = append(resp, p)
		}
	}

	return resp
}
