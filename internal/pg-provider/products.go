package pgprovider

import (
	"context"
	"fmt"
	"godb/internal/models"

	"github.com/jackc/pgx/v4"
	"github.com/segmentio/ksuid"
)

func (i *Instance) AddProduct(ctx context.Context, price int, name, description, categoryID string) {
	commandTag, err := i.Db.Exec(ctx, "INSERT INTO products_v2 (pid, name, price, description, category_id) VALUES ($1, $2, $3, $4, $5)",
		ksuid.New().String(), name, price, description, categoryID)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(commandTag.String())
	fmt.Println(commandTag.RowsAffected())
}

func (i *Instance) GetAllProducts(ctx context.Context) []models.Product {
	var products []models.Product
	rows, err := i.Db.Query(ctx, "SELECT pid, name, price, description, category_id FROM products_v2 WHERE is_deleted = $1;", false)
	if err == pgx.ErrNoRows {
		fmt.Println("No rows")
		return nil
	} else if err != nil {
		fmt.Println(err)
		return nil
	}
	defer rows.Close()
	for rows.Next() {
		product := models.Product{}
		rows.Scan(&product.Pid, &product.Name, &product.Price, &product.Description, &product.CategoryID)
		products = append(products, product)
	}
	fmt.Println(products)
	return products
}

func (i *Instance) GetProductsByCategory(ctx context.Context, category models.Category) []models.Product {
	var products []models.Product
	rows, err := i.Db.Query(ctx, "SELECT pid, name, price, description, category_id FROM products_v2 WHERE category_id =$1 and is_deleted = $2;", category.CategoryID, false)
	if err == pgx.ErrNoRows {
		fmt.Println("No rows")
		return nil
	} else if err != nil {
		fmt.Println(err)
		return nil
	}
	defer rows.Close()
	for rows.Next() {
		product := models.Product{}
		rows.Scan(&product.Pid, &product.Name, &product.Price, &product.Description, &product.CategoryID)
		products = append(products, product)
	}
	fmt.Println(products)
	return products
}

func (i *Instance) GetProductById(ctx context.Context, id int) *models.Product {
	product := &models.Product{}
	err := i.Db.QueryRow(ctx, "SELECT pid, name, price, description, category_id FROM products_v2 WHERE pid=$1 LIMIT 1;", id).
		Scan(&product.Pid, &product.Name, &product.Price, &product.Description, &product.CategoryID)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	fmt.Printf("Product by id: %v\n", product)
	return product
}

func (i *Instance) GetProductByName(ctx context.Context, name string) *models.Product {
	//Выполнение самого запроса. И получение структуры rows, которая содержит в себе строки из базы данных.
	product := &models.Product{}
	//Тут используется метод QueryRow, который предполагает возвращение только одной строки из базы. В нашем случае это гарантирует LIMIT 1 в sql-запросе.
	//Если будет возвращено несколько строк из базы, QueryRaw обработает только одну.
	err := i.Db.QueryRow(ctx, "SELECT pid, name, price, description, category_id FROM products_v2 WHERE name=$1 LIMIT 1;", name).
		Scan(&product.Pid, &product.Name, &product.Price, &product.Description, &product.CategoryID)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	fmt.Printf("Product by name: %v\n", product)
	return product
}
