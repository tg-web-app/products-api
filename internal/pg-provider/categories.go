package pgprovider

import (
	"context"
	"fmt"
	"godb/internal/models"

	"github.com/jackc/pgx/v4"
	"github.com/segmentio/ksuid"
)

func (i *Instance) AddCategory(ctx context.Context, name string) {
	commandTag, err := i.Db.Exec(ctx, "INSERT INTO categories_v2 (cid, name) VALUES ($1, $2)", ksuid.New().String(), name)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(commandTag.String())
	fmt.Println(commandTag.RowsAffected())
}

func (i *Instance) GetAllCategories(ctx context.Context) []models.Category {
	var categories []models.Category
	rows, err := i.Db.Query(ctx, "SELECT cid, name FROM categories_v2 WHERE is_deleted = $1;", false)
	if err == pgx.ErrNoRows {
		fmt.Println("No rows")
		return nil
	} else if err != nil {
		fmt.Println(err)
		return nil
	}
	defer rows.Close()
	for rows.Next() {
		category := models.Category{}
		rows.Scan(&category.CategoryID, &category.CategoryName)
		categories = append(categories, category)
	}
	fmt.Println(categories)
	return categories
}

func (i *Instance) GetCategoryByName(ctx context.Context, name string) *models.Category {
	//Выполнение самого запроса. И получение структуры rows, которая содержит в себе строки из базы данных.
	category := &models.Category{}
	//Тут используется метод QueryRow, который предполагает возвращение только одной строки из базы. В нашем случае это гарантирует LIMIT 1 в sql-запросе.
	//Если будет возвращено несколько строк из базы, QueryRaw обработает только одну.
	err := i.Db.QueryRow(ctx, "SELECT cid, name FROM categories_v2 WHERE name=$1 LIMIT 1;", name).
		Scan(&category.CategoryID, &category.CategoryName)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	fmt.Printf("Category by name: %v\n", category)
	return category
}

func (i *Instance) GetCategoryById(ctx context.Context, categoryID int) *models.Category {
	//Выполнение самого запроса. И получение структуры rows, которая содержит в себе строки из базы данных.
	category := &models.Category{}
	//Тут используется метод QueryRow, который предполагает возвращение только одной строки из базы. В нашем случае это гарантирует LIMIT 1 в sql-запросе.
	//Если будет возвращено несколько строк из базы, QueryRaw обработает только одну.
	err := i.Db.QueryRow(ctx, "SELECT cid, name FROM categories_v2 WHERE cid=$1 LIMIT 1;", categoryID).
		Scan(&category.CategoryID, &category.CategoryName)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	fmt.Printf("Category by name: %v\n", category)
	return category
}
