package pgprovider

import (
	"context"
	"fmt"
	"os"

	"godb/pkg/helpers/pg"

	"github.com/jackc/pgx/v4/pgxpool"
)

type Instance struct {
	cfg *pg.Config
	Db  *pgxpool.Pool
}

func New() *Instance {
	//Задаем параметры для подключения к БД (в прошлом задании мы поднимали контейнер с этими credentials)
	cfg := &pg.Config{}
	cfg.Host = "62.113.105.98"
	cfg.Username = os.Getenv("PG_USER")
	cfg.Password = os.Getenv("PG_PASSWORD")
	cfg.Port = "5432"
	cfg.DbName = "postgres"
	cfg.Timeout = 5
	//Создаем конфиг для пула

	return &Instance{
		cfg: cfg,
	}
}

func (i *Instance) Open() { //TODO: сделать обработку ошибок
	poolConfig, err := pg.NewPoolConfig(i.cfg)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Pool config error: %v\n", err)
		os.Exit(1)
	}
	//Устанавливаем максимальное количество соединений, которые могут находиться в ожидании
	poolConfig.MaxConns = 5
	//Создаем пул подключений
	c, err := pg.NewConnection(poolConfig)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Connect to database failed: %v\n", err)
		os.Exit(1)
	}
	fmt.Println("Connection OK!")
	//Проверяем подключение
	_, err = c.Exec(context.Background(), ";")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Ping failed: %v\n", err)
		os.Exit(1)
	}
	fmt.Println("Ping OK!")
	i.Db = c
}

func (i *Instance) Start() {
	fmt.Println("Project godb started!")

	/*
		i.AddCategory(context.Background(), "Мужское")
		i.AddCategory(context.Background(), "Женское")
		i.AddCategory(context.Background(), "Детское")
		i.AddProduct(context.Background(), 500, "Джинсы", "Made in Turkey", i.GetCategoryByName(context.Background(), "Женское").Cid)
	*/
}

/*
func (i *Instance) updateUserAge(ctx context.Context, name string, age int) {
	_, err := i.Db.Exec(ctx, "UPDATE users SET age=$1 WHERE name=$2;",
		age, name)
	if err != nil {
		fmt.Println(err)
		return
	}
}
*/
