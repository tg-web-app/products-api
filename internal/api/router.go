package api

import (
	pgprovider "godb/internal/pg-provider"
	"godb/internal/service"
)

type Router struct {
	pgProvider *pgprovider.Instance
	s          *service.Service
}

func NewRouter(pgProvider *pgprovider.Instance, s *service.Service) *Router {
	return &Router{
		pgProvider: pgProvider,
		s:          s,
	}
}
