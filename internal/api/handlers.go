package api

import (
	"context"
	"godb/internal/models"
	"net/http"
	"strconv"

	jsoniter "github.com/json-iterator/go"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
)

func (r *Router) Handle(ctx *fasthttp.RequestCtx) { //TODO: сделать через fasthttp сервер
	method, path := string(ctx.Method()), string(ctx.Path())
	switch {
	case path == "/api/v1/categories" && method == fasthttp.MethodGet:
		r.getCategories(ctx)
	case path == "/api/v1/products-by-category" && method == fasthttp.MethodGet: //Deprecated
		r.getProductsByCategory(ctx)
	case path == "/api/v1/products" && method == fasthttp.MethodGet:
		r.getProducts(ctx)
	default:
		ctx.SetStatusCode(http.StatusNotFound)
	}
}

func (r *Router) getCategories(ctx *fasthttp.RequestCtx) {
	ctx.SetContentType("application/json")

	categories := r.pgProvider.GetAllCategories(context.TODO())
	categoriesJson, _ := jsoniter.Marshal(categories)

	ctx.SetBody(categoriesJson)
	ctx.SetStatusCode(fasthttp.StatusOK)
}

// Deprecated
func (r *Router) getProductsByCategory(ctx *fasthttp.RequestCtx) {
	ctx.SetContentType("application/json")
	categoryIdByte := ctx.QueryArgs().Peek("category")
	if len(categoryIdByte) == 0 {
		logrus.Error("cutegoryId not specified")
		return
	}
	categoryIDInt, _ := strconv.Atoi(string(categoryIdByte))

	category := r.pgProvider.GetCategoryById(context.TODO(), categoryIDInt)
	if category == nil {
		logrus.Error("category does not exist")
		return
	}

	products := r.s.GetProductsByCategory(*category)
	productsJson, _ := jsoniter.Marshal(products)
	ctx.SetBody(productsJson)
	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (r *Router) getProducts(ctx *fasthttp.RequestCtx) {
	var products []models.Product
	ctx.SetContentType("application/json")
	categoryIdByte := ctx.QueryArgs().Peek("category")
	productIdByte := ctx.QueryArgs().Peek("id")

	categoryIDInt, _ := strconv.Atoi(string(categoryIdByte))
	productIDInt, _ := strconv.Atoi(string(productIdByte))

	if len(categoryIdByte) != 0 {
		category := r.pgProvider.GetCategoryById(context.TODO(), categoryIDInt)
		if category == nil {
			logrus.Error("category does not exist")
			return
		}

		products = r.s.GetProductsByCategory(*category)
	} else if len(productIdByte) != 0 {
		products = append(products, *r.pgProvider.GetProductById(context.TODO(), productIDInt))
	} else {
		products = r.pgProvider.GetAllProducts(context.Background())
	}
	productsResponse := make([]models.ProductsResponceV2, 0)
	for _, product := range products {
		productResponse := models.ProductsResponceV2{
			Pid:         product.Pid,
			Name:        product.Name,
			Price:       product.Price,
			Description: product.Description,
		}

		category := r.pgProvider.GetCategoryById(context.Background(), product.CategoryID)
		if category != nil {
			productResponse.CategoryName = category.CategoryName
			productResponse.CategoryID = category.CategoryID
		}

		productsResponse = append(productsResponse, productResponse)
	}
	productsJson, _ := jsoniter.Marshal(productsResponse)
	ctx.SetBody(productsJson)
	ctx.SetStatusCode(fasthttp.StatusOK)
}
