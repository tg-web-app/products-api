package models

type Product struct {
	Pid         int    `json:"pid"`
	Name        string `json:"name"`
	Price       int    `json:"price"`
	Description string `json:"description"`
	CategoryID  int    `json:"category_id"`
	IsDeleted   bool   `json:"is_deleted"`
}

type ProductsResponce struct {
	Items        []Product `json:"items"`
	CategoryId   int       `json:"categoryId"`
	CategoryName string    `json:"categoryName"`
}

type ProductsResponceV2 struct {
	Pid          int    `json:"pid"`
	Name         string `json:"name"`
	Price        int    `json:"price"`
	Description  string `json:"description"`
	CategoryName string `json:"category_name"`
	CategoryID   int    `json:"category_id"` //TODO: добавить раздел category
}
