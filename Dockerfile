FROM golang:1.18.8-alpine3.16 as builder
WORKDIR /src
COPY . .
RUN go build -o app ./app

FROM alpine:3.17
WORKDIR /root/
RUN touch checkfile
COPY --from=builder /src/app .
EXPOSE 8080
CMD [ "./app" ]